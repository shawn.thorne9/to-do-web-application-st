from django.urls import path

from . import views

app_name = 'todosApp'
urlpatterns = [
    path('', views.index, name='index'),
    path('save', views.saveTodos, name="saveTodos"),
    path('initialize', views.initialize, name="initialize")
]
