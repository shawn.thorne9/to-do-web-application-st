from datetime import datetime
import json

from django.http import HttpResponse, HttpResponseRedirect
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, render

from .models import TodoList

def index(request):
    context={}
    return render(request, 'todosApp/index.html', context)

def initialize(request):
    # print("This is the session id: " + request.COOKIES)
    if "todoId" in request.session.keys():
        try:
            todoList = TodoList.objects.get(pk=request.session['todoId'])
        except (KeyError, TodoList.DoesNotExist):
            todoList = {"message": "broke it"}
    else:
        todoList = TodoList()
        todoList.save()
        request.session["todoId"] = todoList.id

    return JsonResponse(todoList.todoInfo)

def saveTodos(request):
    print("tried to save")
    if request.method == "POST":
        todoList = get_object_or_404(TodoList, pk=request.session['todoId'])
        todoList.todoInfo = json.loads(request.body.decode('utf-8'))
        todoList.save()
        return JsonResponse({"message": "Success"})
    else:
        return JsonResponse({"message": todoList})
