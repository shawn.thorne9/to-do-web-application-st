const app = Vue.createApp({
    delimiters: ['[[', ']]'],
    data(){
        return{
            "todos": [],
            "saved": "Not Saved",
            "error": false,
            "showPending": true,
            "showComplete": true,
            "todoEditing": null,
        };
    },
    created(){
        let url = "http://127.0.0.1:8000/todosApp/initialize"
        fetch(url)
        .then(r => {
            let rJson = r.json();
            return rJson;
        })
        .then(dat => {
            for(let i of dat['todos']){
                this.todos.push(i);
            }
            this.saved = "Saved"
        }).catch(err => {
            console.log(err);
            this.error = err;
        })
    },
    methods: {
        toggle(todo) {
            todo.isChecked = !todo.isChecked;
            this.save();
        },
        editTodo(todo, newMessage){
            let modifiedMessage = newMessage
            if (modifiedMessage !== null){
                this.todos.at(this.todos.indexOf(todo)).message = modifiedMessage
                this.save();
            };
        },
        addTodo(todoMessage){
            // let todoMessage = document.querySelector("#inputTodo").value;
            this.todos.push({message: todoMessage, isChecked: false});
            this.save();
        },
        removeTodo(todo){
            this.todos.splice(this.todos.indexOf(todo),1);
            this.save()
        },
        showAll(){
            this.showComplete = true;
            this.showPending = true;
        },
        showAllPending(){
            this.showPending = true;
            this.showComplete = false;
        },
        showAllComplete(){
            this.showPending = false;
            this.showComplete = true;
        },
        save(){
            let toSave = {}
            toSave['todos'] = this.todos;
            this.saved = "Saving";
            const csrftoken = document.querySelector('[name=csrfmiddlewaretoken]').value;
            let URL = "http://127.0.0.1:8000/todosApp/save";
            fetch(URL, {
                method: 'POST',
                headers: { 
                    'X-CSRFToken': csrftoken,
                    'Content-Type': 'application/json',
                    "Connection": "keep-alive",
                },
                body: JSON.stringify(toSave),
            })
            .then((r) => r.json())
            .then(r => {
                this.saved = "Saved";
            })
            .catch(err => {
                this.error = err;
                this.saved = "Unable to Save";
            })
        },
        isToShow(todo){
            if(todo.isChecked === this.showComplete){
                return true;
            } else if (!todo.isChecked === this.showPending){
                return true;
            } else {
                return false;
            }
        },
        showPopup(message, todo=null){
            document.getElementById("inputMessage").textContent = message;
            if(todo !== null){
                document.getElementById("inputTodo").value = todo.message;
                this.todoEditing = todo;
            }
            document.getElementById("popupDiv").classList = "popupVisible";
            document.getElementById("inputTodo").focus();
            document.getElementById("inputTodo").select();

        },
        closePopup(saveTodo=false){
            if(saveTodo){
                if(this.todoEditing !== null){
                    let newMessage = document.getElementById("inputTodo").value;
                    this.editTodo(this.todoEditing, newMessage);
                } else {
                    this.addTodo(document.querySelector("#inputTodo").value);
                }
            }
            document.getElementById("popupDiv").classList = "popupHidden";
            this.todoEditing = null;
            document.querySelector("#inputTodo").value = "";
        }
    }

});


const vm = app.mount('#app');