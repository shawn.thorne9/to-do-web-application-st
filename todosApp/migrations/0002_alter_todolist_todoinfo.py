# Generated by Django 4.1.2 on 2023-01-12 04:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todosApp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todolist',
            name='todoInfo',
            field=models.JSONField(default={}),
        ),
    ]
